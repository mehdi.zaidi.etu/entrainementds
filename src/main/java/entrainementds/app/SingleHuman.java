package entrainementds.app;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Cette classe represente un Humain.
 * Dans notre application, l'Humain est un singleton car il n'existe qu'un seul et unique humain.
 * Il est impossible de créer un humain avec "new Human()", le constructeur est donc privé.
 * Affin de récuperer l'instance de notre Human nous passons par une methode statique
 * Cette méthode assure qu'il n'y ait pas un humain déjà créé, dans le cas contraire elle renvoie ce même humain
 * @author Hido
 *
 */
public class SingleHuman {
	static SingleHuman instance = null;
	
	static SingleHuman getInstance() {
		if(SingleHuman.instance == null) {
			SingleHuman.instance = new SingleHuman();
		}
		return SingleHuman.instance;
	}
	
	protected final StringProperty name = new SimpleStringProperty();
	protected final String[] names = new String[] {"Leonce", "Gabriel", "Candido", "Pierre", "Gabriel", "Elisha", "Henry", "Chiara", "Gabriel", "Luca", "Aureliane", "Evariste"};
	
	private SingleHuman() {
		this.setRandomName();
	}
	
	public String getPrenom() {
		return this.name.getValue();
	}
	
	public void setPrenom(String prenom) {
		this.name.setValue(prenom);
	}
	
	public StringProperty getProperty() {
		return this.name;
	}

	public void setRandomName() {
		this.setPrenom(this.names[(int) (Math.random()*this.names.length)]);
	}
}
