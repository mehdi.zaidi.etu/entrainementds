package entrainementds.app;

/**
 * Le controller d'une vue est la classe s'occupant de toutes les intéractions entre une vue et un modele.
 * Pour rappel :
 * La vue = la partie graphique affiché à l'utilisateur
 * Le modele = la ressource contenant les données que l'on affiche sur la vue (à l'utilisateur)
 * 
 * Pour faire simple, le principe du MVC est de séparer le code en 3 parties.
 * Chaque partie s'occupera d'un traitement bien spécifique de l'application.
 * La vue affiche les informations (les composants javaFX dans notre cas)
 * Le controlleur sert d'interface entre la vue et le model (il est le pigeon voyageur)
 * Le modele sert principalement à stocker les données et propose des fonctionnalités (méthodes) sur ces données.
 * 
 * ATTENTION : Dans aucun cas une des partie du MVC doit faire le travail d'une des autres parties.
 * (ex: Gérer la modification du modèle lors du clic sur un bouton de la vue directement dans la vue)
 * @author Hido
 *
 */
public class Controller {
	// Attribut contenant la vue associé au Controller
	protected View view;
	
	// Attribut contenant le modèle associé au Controller (et donc la view)
	protected SingleHuman model;
	
	// Lors de la construction d'un controlleur, il prend en paramètre la view qu'il doit gérer
	public Controller(View view) {
		// Il instancie un nouveau modèle (ici un SingleHuman, qui est un Singleton)
		this.model = SingleHuman.getInstance();
		
		// Il va "bind" le label affichant le nom de l'humain (de la view) au nom du SingleHumain (le model)
		// Cette ligne correspond au "subject.attach(observable)" vu en TP.
		// Vous ne trouverez pas de "update" ou encore de "notify"
		// car ils sont gérés en interne des composants Java (StringProperty) & JavaFX (Label.textProperty)
		view.nameLabel.textProperty().bind(this.model.getProperty());
		
		// Ici on définie des intéractions avec les différents composants JavaFX
		btnRandomNameOnActionHandler(view);
		nameTextFieldKeyTypedHandler(view);
	}
	
	// Action récupéré lors de l'interaction avec le bouton de la view
	private void btnRandomNameOnActionHandler(View view) {
		view.randomBtnGenerator.setOnAction((e)-> {
			this.model.setRandomName();
		});
	}

	// Action récupéré lors de la saisie de texte dans le TextField
	private void nameTextFieldKeyTypedHandler(View view) {
		view.nameTextField.setOnKeyTyped((e) -> {
			this.model.setPrenom(view.nameTextField.getText());
		});
	}
}
