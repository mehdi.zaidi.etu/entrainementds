package entrainementds.app;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class View extends Application {
	protected Label nameLabel;
	protected Button randomBtnGenerator;
	protected TextField nameTextField;
	protected Controller controller;
	
	public View() {
		this.nameLabel = new Label();
		this.randomBtnGenerator = new Button("Random rename");
		this.nameTextField = new TextField();
		this.controller = new Controller(this);
	}
	
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
    	primaryStage.setTitle("Simple MVC");
    	
        VBox vbox = new VBox();
        vbox.setSpacing(20);
        vbox.getChildren().addAll(this.nameLabel, this.nameTextField, this.randomBtnGenerator);
        this.registerVBoxInsets(vbox);
        
        StackPane root = new StackPane();
        root.getChildren().add(vbox);
        
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }

	private void registerVBoxInsets(VBox vbox) {
		for(Node child : vbox.getChildren()) {
        	vbox.setMargin(child, new Insets(20,20,20,20));
        }
	}
}