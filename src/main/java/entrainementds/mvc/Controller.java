package entrainementds.mvc;

import java.util.Scanner;

public class Controller<T> {
	protected ViewObserver<T> view;
	protected ModelSubject<T> model;
	
	public Controller(ViewObserver<T> view) {
		this.view = view;
		this.model = new ModelSubject<T>();
		this.model.attach(this.view);
	}
	
	@SuppressWarnings("unchecked")
	public void askNewModelValue() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter new value: ");
		this.model.setValue((T) sc.next());
		sc.close();
	}
}
