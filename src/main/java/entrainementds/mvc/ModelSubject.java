package entrainementds.mvc;

import java.util.ArrayList;
import java.util.List;

import entrainementds.mvc.interfaces.IObservable;
import entrainementds.mvc.interfaces.IObserver;

public class ModelSubject<T> implements IObservable<T> {
	protected List<IObserver<T>> observers;
	protected T value;
	
	public ModelSubject() {
		this.observers = new ArrayList<>();
	}
	
	public T getValue() { return this.value; }
	
	public void setValue(T value) {
		this.value = value;
		this.notifyObservers(value);
	}
	
	@Override
	public void attach(IObserver<T> observer) {
		this.observers.add(observer);
	}

	@Override
	public void detach(IObserver<T> observer) {
		this.observers.remove(observer);
	}

	@Override
	public void notifyObservers(T value) {
		for(IObserver<T> observer : this.observers) {
			observer.update(value);
		}
	}

}
