package entrainementds.mvc.interfaces;

public interface IObserver<T> {
	void update(T newValue);
}
