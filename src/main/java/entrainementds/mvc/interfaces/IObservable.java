package entrainementds.mvc.interfaces;

public interface IObservable<T> {
	void attach(IObserver<T> observer);
	void detach(IObserver<T> observer);
	void notifyObservers(T value);
}
