package entrainementds.mvc;

import entrainementds.mvc.interfaces.IObserver;

public class ViewObserver<T> implements IObserver<T> {
	protected T value;
	protected Controller<T> controller;
	
	public ViewObserver() {
		this.controller = new Controller<T>(this);
	}
	
	public T getValue() {
		return this.value;
	}
	
	public void setValue(T value) {
		System.out.println(String.format("%s has changed for %s", this.value, value));
		this.value = value;
	}
	
	@Override
	public void update(T newValue) {
		this.setValue(newValue);
	}
	
	
	public static void main(String[] args) {
		ViewObserver<Integer> view = new ViewObserver<>();
		view.controller.askNewModelValue();
	}
}
