package entrainementds.app;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SingleHumanTest {

	@Test
	void testHumanSingleton() {
		assertTrue(SingleHuman.getInstance() == SingleHuman.getInstance());
	}

}
