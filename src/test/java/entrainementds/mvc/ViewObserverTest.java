package entrainementds.mvc;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;

class ViewObserverTest {

	@Test
	void test() {
		ModelSubject<Integer> model = new ModelSubject<>();
		model.setValue(50);
		ViewObserver<Integer> view = new ViewObserver<>();
		view.setValue(30);
		assertFalse(model.getValue().equals(view.getValue()));

		model.attach(view);
		model.setValue(40);
		assertTrue(model.getValue().equals(view.getValue()));
	}

}
