# MVC pour le CTP du 26/10/2022

### N'ayant jamais fait de MVC dans les TP mais un simple "MV", voila un exemple.
### Je ne suis pas un prof, je n'ai pas la science infuse, il y a peut être des erreurs.
### Dans le doute faites le valider par un prof.

# Test singleton:
[![testsingleton](https://i.gyazo.com/95f040c7941ae92a187c4f08b15451fe.png)](https://gyazo.com/95f040c7941ae92a187c4f08b15451fe)

# MVC demo :
[![mvcdemo](https://i.gyazo.com/1a45b05515706c92717f997d337d7771.gif)](https://gyazo.com/1a45b05515706c92717f997d337d7771)